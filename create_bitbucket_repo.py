import sys
import os
import json
import subprocess
from utils import run_shell_command_silently

from create_empty_bitbucket_repo import create_empty_bitbucket_repository

expected_format = sys.argv[0] + \
    " <username> <password> <workspace> <project> <repository_name>"


def build_congratulation_message(repository_name):
    return "Congratulation!!! The " + repository_name + " repo has been successfully created."


def decorate_message(message):
    return "#######  " + message + "  #######"


def create_bitbucket_repository(username, password, workspace, project, repository_name):
    create_empty_bitbucket_repository(
        username, password, workspace, project, repository_name, False)

    repository_url = subprocess.run(
        ["git remote get-url origin"], shell=True, capture_output=True).stdout.decode("utf-8")

    print(decorate_message("Adding files to repository"))
    os.system("git add .")

    print(decorate_message("Commiting first changes"))
    run_shell_command_silently("git commit -m \"First commit\"")

    # Replace username with username:password, so when executing this script
    # git doesn't show the prompt asking for the password
    repository_url_with_password = repository_url.replace(
        username, username + ":" + password, 1).replace("\n", "")

    print(decorate_message("Pushing first commit"))
    # os.system("git push --set-upstream " +
    #           repository_url_with_password + " master")
    os.system("git push --set-upstream origin master")

    print("")
    print(build_congratulation_message(repository_name))


def main():
    args = sys.argv
    username = ""
    password = ""
    workspace = ""
    project = ""
    repository_name = ""
    if len(args) == 6:
        username = args[1]
        password = args[2]
        workspace = args[3]
        project = args[4]
        repository_name = args[5]

        if os.path.isdir(repository_name):
            create_bitbucket_repository(
                username, password, workspace, project, repository_name)
        else:
            print(repository_name + " directory doesn't exist")
    else:
        print("The expected format is:\n" + "\t" + expected_format)


if __name__ == "__main__":
    main()
