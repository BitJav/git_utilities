import subprocess


def run_shell_command_silently(command):
    subprocess.run([command], shell=True, stdout=subprocess.DEVNULL)


def build_congratulation_message(repository_name):
    return "Congratulation!!! The " + repository_name + " repo has been successfully created."


def decorate_message(message):
    return "#######  " + message + "  #######"
