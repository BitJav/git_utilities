import sys
import os
import json
import subprocess
from utils import run_shell_command_silently, decorate_message, build_congratulation_message

expected_format = sys.argv[0] + \
    " <username> <password> <workspace> <project> <repository_name>"


set_upstream_help = "After commiting your first changes you'll have to configure the\n" + \
    "upstream branch in order to link your local branch with the remote one.\n" + \
    "This can be done with by pushing your commits as follows:\n\t" + \
    "git push --set-upstream origin master"


def create_empty_bitbucket_repository(username, password, workspace, project, repository_name, directory_exist):
    call_git_repo_creation = "curl POST -H \"Content-Type: application/json\" -v -u " + username + ":" + password + \
        " -d \'{\"scm\": \"git\", \"is-private\": \"true\", \"fork-policy\": \"no_public_forks\", \"project\": {\"key\": \"" + \
        project + "\"} }\' " + "https://api.bitbucket.org/2.0/repositories/" + \
        workspace + "/" + repository_name

    bitbucket_response_string = subprocess.run(
        [call_git_repo_creation], shell=True, capture_output=True)

    bitbucket_response = json.loads(bitbucket_response_string.stdout)
    if bitbucket_response["type"] != "error":
        repository_url = bitbucket_response["links"]["clone"][0]["href"]

        if directory_exist:
            print(decorate_message("Creating the " +
                                   repository_name + " directory"))
            os.system("mkdir " + repository_name)

        os.chdir(repository_name)

        print(decorate_message("Initializing " +
                               repository_name + " as a git repo"))
        run_shell_command_silently("git init")

        print(decorate_message("Setting origin remote as " + repository_url))
        os.system("git remote add origin " + repository_url)

        if directory_exist:
            print("")
            print(build_congratulation_message(repository_name))
            print(set_upstream_help)
    else:
        print("There were errors while creating the repository:")
        print("\t" + str(bitbucket_response["error"]["fields"]))


def main():
    args = sys.argv
    username = ""
    password = ""
    workspace = ""
    project = ""
    repository_name = ""
    if len(args) == 6:
        username = args[1]
        password = args[2]
        workspace = args[3]
        project = args[4]
        repository_name = args[5]
        create_empty_bitbucket_repository(
            username, password, workspace, project, repository_name, True)
    else:
        print("The expected format is:\n" + "\t" + expected_format)


if __name__ == "__main__":
    main()
