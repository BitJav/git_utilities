# Utilidades GIT

These project is meant to gather different functionalities about git in order to simplify some actions usign Bitbucket repositories.

## Pre-requisites 🔧

python3 needs to be installed in the operating system.

To install it in ubuntu run the following command.

```
sudo apt install python3.8
```

## Commands

* create_empty_bitbucket_repo: creates an empty repository both, local and remotely.
* create_bitbucket_repo: creates a repository both, local and remotely, based on an existing directory with data in it.

## Usage

In the next sections it will be explained how each command is meant to be used

### craete_empty_bitbucket_repo

This script creates the following elements:

1. A Bitbucket repository named _<repository_name>_
2. A local repository named _<repository_name>_

Then sets the Bitbucket repository as a remote named origin.

```
python3 create_empty_bitbucket_repo.py <username> <password> <workspace> <project> <repository_name>
```
where

* _username_: bitbucket's username
* _password_: bitbucket's password
* _workspace_: workspace where is meant to create the repository
* _project_: project where is meant to create the repository
* _repository_name_: repository name

**Note**: As it is an empty repository there is no way to link the local master branch to the remote one (because samewhat they don't exist yet). So when some files are added and commited it will be necessary to push them using the _--set-upstream_ flag like below:
```
git push --set-upstream origin master
```

### craete_bitbucket_repo
It is supposed that a directory named _<repository_name>_ exist and has some files in it.

This script does the following:

1. Creates a Bitbucket repository named _<repository_name>_
2. Creates a local repository named _<repository_name>_
3. Adds all the files inside the _<repository_name>_ directory
4. Generates the first commit
5. Push the commit to the Bitbucket repo


```
python3 create_bitbucket_repo.py <username> <password> <workspace> <project> <repository_name>
```
where

* _username_: bitbucket's username
* _password_: bitbucket's password
* _workspace_: workspace where is meant to create the repository
* _project_: project where is meant to create the repository
* _repository_name_: repository name